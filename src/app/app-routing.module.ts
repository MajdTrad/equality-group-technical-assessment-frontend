import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';

import { StarshipsComponent } from './starships/starships.component';

const routes: Routes = [
  {path:'starships',component:StarshipsComponent},
  {path:'home',redirectTo:"/"}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
