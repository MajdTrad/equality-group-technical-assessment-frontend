import { TestBed } from '@angular/core/testing';

import { ConsumeAPIService } from './consume-api.service';

describe('ConsumeAPIService', () => {
  let service: ConsumeAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsumeAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
