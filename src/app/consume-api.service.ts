import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConsumeAPIService {

  readonly APIUrl = "http://localhost:2921/api";
  constructor(private http:HttpClient) { }

  getNumberOfStops(distance:string):Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/values/?distance='+distance);
  }
}
