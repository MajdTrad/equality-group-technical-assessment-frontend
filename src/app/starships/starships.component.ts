import { Component, OnInit } from '@angular/core';
import {ConsumeAPIService} from 'src/app/consume-api.service';
@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.css']
})
export class StarshipsComponent implements OnInit {

  constructor(private service:ConsumeAPIService) { }

  StarshipList:any=[];
  ngOnInit(): void {
  }

  GetStarshipsStops(distance:string){
    if(distance==null || distance== "0" || distance == "")
    {
      alert("Please enter a valid distance value")
    }
    else
    {
      this.service.getNumberOfStops(distance).subscribe(data=>{
        this.StarshipList=data;
      })
    }
  }
}
